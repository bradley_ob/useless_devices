package controllers
import java.util.Date
case class EventForm (
                     title: String,
                     description: String,
                     startDate: java.util.Date,
                     endDate: java.util.Date,
                     recurrenceScheme: String
                     )
object EventForm {
  import play.api.data.Forms._
  import play.api.data.Form
  import models.Event

  val form = Form(
    mapping(
      "title" -> nonEmptyText,
      "description" -> text,
      "startDate" -> date,
      "endDate" -> date,
      "recurrenceScheme" -> nonEmptyText,
    )(EventForm.apply)(EventForm.unapply)
  )
}