package controllers

import javax.inject._
import play.api._
import play.api.mvc._

import models.Event

/**
  * Home page route
  */
@Singleton
class EventController @Inject()(cc: ControllerComponents, event: Event) extends AbstractController(cc) {
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.events())
  }
  //def createEvent() = Action { implicit request => new Event}
}
