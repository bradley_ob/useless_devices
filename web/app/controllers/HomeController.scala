package controllers

import javax.inject._
import play.api._
import play.api.i18n._
import play.api.mvc._
import java.time.format.DateTimeFormatter
/**
  * Home page route
  */
@Singleton
class HomeController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {
  import EventForm._
  import models.{Event, Recurrence}

  def index() = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.index(form))
  }

  def echo() = Action { implicit request: MessagesRequest[AnyContent] =>
    form.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.index(formWithErrors))
      },
      event => {
        val userEvent = Event(
          9,
          event.title,
          event.description,
          event.startDate,
          event.endDate,
          Recurrence(event.recurrenceScheme),
          99
        )
        Ok(event.startDate.toString())
      }
    )
  }
}
