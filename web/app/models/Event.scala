package models

//import java.security.Timestamp
import java.time
import java.sql.Timestamp
import java.time.format.DateTimeFormatter
import java.util.Calendar

import javax.inject._
import java.util.Date

/**
  * Encapsulates a single event in the partitioned events table
  */
case class Event ( id: Integer, title: String, description: String, startDate: Date, endDate: Date, recurrenceScheme: Recurrence, userId: Long ) {

}

object Event {
  def calculateNextDate(startDate: Date, recurrenceScheme: Recurrence): Date = {
    val cal = Calendar.getInstance()
    cal.setTime(startDate)
    recurrenceScheme.scheme match {
      case "hourly" => cal.add(Calendar.HOUR, 1); cal.getTime
      case "daily" => cal.add(Calendar.DAY_OF_WEEK, 1); cal.getTime
      case "weekly" => cal.add(Calendar.DAY_OF_WEEK, 7); cal.getTime
      case "biweekly" => cal.add(Calendar.DAY_OF_WEEK, 14); cal.getTime
      case "monthly" => cal.add(Calendar.MONTH, 1); cal.getTime
      case "yearly" => cal.add(Calendar.YEAR, 1); cal.getTime
    }
  }
}

