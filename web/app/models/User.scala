package models

case class User (
                  id: Integer,
                  username: String,
                  name: String,
                  password: String
                )
