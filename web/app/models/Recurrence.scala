package models

/**
  * A recurrence can be hourly, daily, weekly, monthly, or yearly
  * @param scheme
  */
case class Recurrence(scheme: String) {
  require(List("hourly", "daily", "weekly", "yearly").contains(scheme), "Incorrect recurrence type")
}

