name := """web"""
organization := "es.bradleybarn"
scalaVersion := "2.12.8"
version := "1.0-SNAPSHOT"

libraryDependencies += guice
lazy val root = (project in file(".")).enablePlugins(PlayScala, PlayEbean)

